use anyhow::Result;

fn main() -> Result<()> {
    let puzzle_input = aoc_core::get_input(2015, 2)?;

    let total_paper_needed: u32 = puzzle_input
        .lines()
        .map(|line| {
            let dimensions: Dimensions = line.into();
            calculate_paper_needed(&dimensions)
        })
        .sum();

    println!("Total paper needed: {}", total_paper_needed);
    
    let total_ribbon_needed: u32= puzzle_input
        .lines()
        .map(|line| {
            let dimensions: Dimensions = line.into();
            calculate_ribbon_needed(&dimensions)
        })
        .sum();

    println!("Total ribbon needed: {}", total_ribbon_needed);

    Ok(())
}

fn calculate_paper_needed(dimensions: &Dimensions) -> u32 {
    let surface_area = dimensions.surface_area();
    let smallest_side_area = dimensions.smallest_side_area();

    surface_area + smallest_side_area
}

fn calculate_ribbon_needed(dimensions: &Dimensions) -> u32 {
    let ribbon_length_for_wrapping = dimensions.smallest_perimeter_side();
    let ribbon_length_for_bow = dimensions.cubic_length();

    ribbon_length_for_wrapping + ribbon_length_for_bow
}

struct Dimensions {
    width: u32,
    height: u32,
    length: u32,
}

impl From<&str> for Dimensions {
    fn from(s: &str) -> Self {
        let parts: Vec<&str> = s.split('x').collect();

        Dimensions {
            width: parts[1].parse().unwrap(),
            height: parts[2].parse().unwrap(),
            length: parts[0].parse().unwrap(),
        }
    }
}

impl Dimensions {
    pub fn surface_area(&self) -> u32 {
        let side1 = self.width * self.height;
        let side2 = self.width * self.length;
        let side3 = self.height * self.length;

        2 * side1 + 2 * side2 + 2 * side3
    }

    pub fn smallest_side_area(&self) -> u32 {
        let side1 = self.width * self.height;
        let side2 = self.width * self.length;
        let side3 = self.height * self.length;

        side1.min(side2).min(side3)
    }

    pub fn smallest_perimeter_side(&self) -> u32 {
        let left_face = self.height * 2 + self.length * 2;
        let top_face = self.width * 2 + self.length * 2;
        let front_face = self.height * 2 +  self.width * 2;

        left_face.min(top_face).min(front_face)
    }

    pub fn cubic_length(&self) -> u32 {
        self.height * self.width * self.length
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn from_str_should_return_2lx3wx4h_for_2x3x4() {
        let dimensions: Dimensions = "2x3x4".into();

        assert_eq!(dimensions.length, 2);
        assert_eq!(dimensions.width, 3);
        assert_eq!(dimensions.height, 4);
    }

    #[test]
    fn from_str_should_return_1lx1wx10h_for_1x1x10() {
        let dimensions: Dimensions = "1x1x10".into();

        assert_eq!(dimensions.length, 1);
        assert_eq!(dimensions.width, 1);
        assert_eq!(dimensions.height, 10);
    }

    #[test]
    fn surface_area_should_return_52_for_2x3x4() {
        let dimensions: Dimensions = "2x3x4".into();

        assert_eq!(dimensions.surface_area(), 52);
    }

    #[test]
    fn surface_area_should_return_42_for_1x1x10() {
        let dimensions: Dimensions = "1x1x10".into();

        assert_eq!(dimensions.surface_area(), 42);
    }

    #[test]
    fn smallest_side_area_should_return_6_for_2x3x4() {
        let dimensions: Dimensions = "2x3x4".into();

        assert_eq!(dimensions.smallest_side_area(), 6);
    }

    #[test]
    fn smallest_side_area_should_return_1_for_1x1x10() {
        let dimensions: Dimensions = "1x1x10".into();

        assert_eq!(dimensions.smallest_side_area(), 1);
    }

    #[test]
    fn calculate_paper_needed_should_return_58_for_2x3x4() {
        let dimensions: Dimensions = "2x3x4".into();

        assert_eq!(calculate_paper_needed(&dimensions), 58);
    }

    #[test]
    fn calculate_paper_needed_should_return_43_for_1x1x10() {
        let dimensions: Dimensions = "1x1x10".into();

        assert_eq!(calculate_paper_needed(&dimensions), 43);
    }

    #[test]
    fn smallest_perimeter_side_should_return_10_for_2x3x4() {
        let dimensions: Dimensions = "2x3x4".into();

        assert_eq!(dimensions.smallest_perimeter_side(), 10);
    }

    #[test]
    fn smallest_perimeter_side_should_return_4_for_1x1x10() {
        let dimensions: Dimensions = "1x1x10".into();

        assert_eq!(dimensions.smallest_perimeter_side(), 4);
    }

    #[test]
    fn cubic_length_should_return_24_for_2x3x4() {
        let dimensions: Dimensions = "2x3x4".into();

        assert_eq!(dimensions.cubic_length(), 24);
    }

    #[test]
    fn cubic_length_should_return_10_for_1x1x10() {
        let dimensions: Dimensions = "1x1x10".into();

        assert_eq!(dimensions.cubic_length(), 10);
    }

    #[test]
    fn calculate_ribbon_needed_should_return_34_for_2x3x4() {
        let dimensions: Dimensions = "2x3x4".into();

        assert_eq!(calculate_ribbon_needed(&dimensions), 34);
    }

    #[test]
    fn calculate_ribbon_needed_should_return_14_for_1x1x10() {
        let dimensions: Dimensions = "1x1x10".into();

        assert_eq!(calculate_ribbon_needed(&dimensions), 14);
    }
}
