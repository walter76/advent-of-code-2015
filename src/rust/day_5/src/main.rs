use anyhow::Result;

fn main() -> Result<()>{
    let puzzle_input = aoc_core::get_input(2015, 5)?;

    let number_of_nice_strings = nice_strings_part_1::count_nice_strings(&puzzle_input);
    println!("Number of nice strings: {}", number_of_nice_strings);

    let number_of_nice_strings = nice_strings_part_2::count_nice_strings(&puzzle_input);
    println!("Number of nice strings: {}", number_of_nice_strings);
    
    Ok(())
}

mod nice_strings_part_2 {
    pub fn count_nice_strings(input: &str) -> usize {
        input.lines().filter(|line| is_nice(line)).count()
    }

    fn is_nice(s: &str) -> bool {
        has_repeating_pair(s) && has_repeating_letter_with_one_between(s)
    }

    fn has_repeating_pair(s: &str) -> bool {
        let mut pairs = s.chars().zip(s.chars().skip(1));
        pairs.any(|(a, b)| s.matches(&format!("{}{}", a, b)).count() > 1)
    }

    fn has_repeating_letter_with_one_between(s: &str) -> bool {
        s.chars().zip(s.chars().skip(2)).any(|(a, b)| a == b)
    }

    #[cfg(test)]
    mod test {
        use crate::nice_strings_part_2::{has_repeating_letter_with_one_between, is_nice};

        use super::has_repeating_pair;

        #[test]
        fn has_repeating_pair_should_return_false_for_string_without_repeating_pair() {
            assert_eq!(false, has_repeating_pair("xyx"));
        }

        #[test]
        fn has_repeating_pair_should_return_true_for_string_with_repeating_pair() {
            assert_eq!(true, has_repeating_pair("xyxy"));
        }

        #[test]
        fn has_repeating_pair_should_return_true_for_string_with_multiple_repeating_pairs() {
            assert_eq!(true, has_repeating_pair("aabcdefgaa"));
        }

        #[test]
        fn has_repeating_pair_should_return_false_for_string_with_no_repeating_pairs() {
            assert_eq!(false, has_repeating_pair("aaa"));
        }

        #[test]
        fn has_repeating_letter_with_one_between_should_return_true_for_efe() {
            assert_eq!(true, has_repeating_letter_with_one_between("abcdefeghi"));
        }

        #[test]
        fn has_repeating_letter_with_one_between_should_return_false_for_abc() {
            assert_eq!(false, has_repeating_letter_with_one_between("abc"));
        }

        #[test]
        fn has_repeating_letter_with_one_between_should_return_true_for_aaa() {
            assert_eq!(true, has_repeating_letter_with_one_between("aaa"));
        }

        #[test]
        fn is_nice_should_return_true_for_pair_that_appears_twice_and_a_letter_that_repeats_exactly_one() {
            assert_eq!(true, is_nice("qjhvhtzxzqqjkmpb"));
        }

        #[test]
        fn is_nice_should_return_true_for_pair_that_appears_twice_and_a_letter_that_repeats_exactly_one_although_overlap() {
            assert_eq!(true, is_nice("xxyxx"));
        }

        #[test]
        fn is_nice_should_return_false_for_pair_that_appears_twice_but_no_letter_that_repeats_exactly_one() {
            assert_eq!(false, is_nice("uurcxstgmygtbstg"));
        }

        #[test]
        fn is_nice_should_return_false_for_pair_that_appears_twice_but_no_letter_that_repeats_exactly_one_although_overlap() {
            assert_eq!(false, is_nice("ieodomkazucvgmuy"));
        }
    }
}

mod nice_strings_part_1 {
    pub fn count_nice_strings(input: &str) -> usize {
        input.lines().filter(|line| is_nice(line)).count()
    }
    
    fn is_nice(s: &str) -> bool {
        has_at_least_three_vowels(s) && has_double_letters(s) && does_not_contain_forbidden_strings(s)
    }
    
    fn has_at_least_three_vowels(s: &str) -> bool {
        count_vowels(s) >= 3
    }
    
    fn count_vowels(s: &str) -> usize {
        s.chars().filter(|c| "aeiou".contains(*c)).count()
    }
    
    fn has_double_letters(s: &str) -> bool {
        s.chars().zip(s.chars().skip(1)).any(|(a, b)| a == b)
    }
    
    fn does_not_contain_forbidden_strings(s: &str) -> bool {
        !["ab", "cd", "pq", "xy"].iter().any(|forbidden| s.contains(forbidden))
    }
    
    #[cfg(test)]
    mod test {
        use super::{count_vowels, does_not_contain_forbidden_strings, has_at_least_three_vowels, has_double_letters, is_nice};
    
        #[test]
        fn count_vowels_should_return_0_for_empty_string() {
            assert_eq!(0, count_vowels(""));
        }
    
        #[test]
        fn count_vowels_should_return_0_for_string_without_vowels() {
            assert_eq!(0, count_vowels("xyz"));
        }
    
        #[test]
        fn count_vowels_should_return_1_for_string_with_one_vowel() {
            assert_eq!(1, count_vowels("xax"));
        }
    
        #[test]
        fn count_vowels_should_return_3_for_string_with_three_vowels() {
            assert_eq!(3, count_vowels("xaxexix"));
        }
    
        #[test]
        fn has_at_least_three_vowels_should_return_false_for_string_with_two_vowels() {
            assert_eq!(false, has_at_least_three_vowels("xaxex"));
        }
    
        #[test]
        fn has_at_least_three_vowels_should_return_true_for_string_with_three_vowels() {
            assert_eq!(true, has_at_least_three_vowels("xaxexixu"));
        }
    
        #[test]
        fn has_double_letters_should_return_false_for_string_without_double_letters() {
            assert_eq!(false, has_double_letters("abc"));
        }
    
        #[test]
        fn has_double_letters_should_return_true_for_string_with_double_letters() {
            assert_eq!(true, has_double_letters("abbc"));
        }
    
        #[test]
        fn does_not_contain_forbidden_strings_should_return_true_for_string_without_forbidden_strings() {
            assert_eq!(true, does_not_contain_forbidden_strings("def"));
        }
    
        #[test]
        fn does_not_contain_forbidden_strings_should_return_false_for_string_with_forbidden_strings() {
            assert_eq!(false, does_not_contain_forbidden_strings("abxyz"));
        }
    
        #[test]
        fn is_nice_should_return_true_for_ugknbfddgicrmopn() {
            assert_eq!(true, is_nice("ugknbfddgicrmopn"));
        }
    
        #[test]
        fn is_nice_should_return_true_for_aaa() {
            assert_eq!(true, is_nice("aaa"));
        }
    
        #[test]
        fn is_nice_should_return_false_for_jchzalrnumimnmhp() {
            assert_eq!(false, is_nice("jchzalrnumimnmhp"));
        }
    
        #[test]
        fn is_nice_should_return_false_for_haegwjzuvuyypxyu() {
            assert_eq!(false, is_nice("haegwjzuvuyypxyu"));
        }
    
        #[test]
        fn is_nice_should_return_false_for_dvszwmarrgswjxmb() {
            assert_eq!(false, is_nice("dvszwmarrgswjxmb"));
        }
    
        #[test]
        fn count_nice_strings_should_return_2_for_example_input() {
            let input =
r"ugknbfddgicrmopn
aaa
jchzalrnumimnmhp
haegwjzuvuyypxyu
dvszwmarrgswjxmb";
    
            assert_eq!(2, super::count_nice_strings(input));
        }
    }
}
