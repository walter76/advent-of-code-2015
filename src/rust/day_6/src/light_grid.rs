use aoc_core::{char_grid::CharGrid, primitives::Position};

const LIGHT_OFF: char = 'o';
const LIGHT_ON: char = 'x';

const GRID_WIDTH: usize = 1000;
const GRID_HEIGHT: usize = 1000;

pub struct LightGrid {
    lights: CharGrid,
}

impl LightGrid {
    pub fn new() -> Self {
        Self {
            lights: CharGrid::new(GRID_WIDTH, GRID_HEIGHT, LIGHT_OFF),
        }
    }

    pub fn turn_on(&mut self, top_left: Position, bottom_right: Position) {
        self.lights.fill_rect(top_left, bottom_right, LIGHT_ON);
    }

    pub fn turn_off(&mut self, top_left: Position, bottom_right: Position) {
        self.lights.fill_rect(top_left, bottom_right, LIGHT_OFF);
    }

    pub fn toggle(&mut self, top_left: Position, bottom_right: Position) {
        for y in top_left.y..=bottom_right.y {
            for x in top_left.x..=bottom_right.x {
                match self.lights.get(Position { x, y }) {
                    LIGHT_ON => self.lights.set(Position { x, y }, LIGHT_OFF),
                    LIGHT_OFF => self.lights.set(Position { x, y }, LIGHT_ON),
                    _ => unreachable!(),
                }
            }
        }
    }

    pub fn count_lights_on(&self) -> usize {
        self.lights.count_occurrences(LIGHT_ON)
    }
}

#[cfg(test)]
mod test {
    use aoc_core::primitives::Position;
    use super::LightGrid;

    #[test]
    fn new_should_create_grid_with_correct_size() {
        let grid = LightGrid::new();
        
        assert_eq!(grid.lights.width(), 1000);
        assert_eq!(grid.lights.height(), 1000);
    }

    #[test]
    fn new_should_create_grid_with_all_lights_off() {
        let grid = LightGrid::new();
        
        assert_eq!(grid.lights.count_occurrences('o'), 1000 * 1000);
    }

    #[test]
    fn turn_on_should_turn_on_lights_in_rectangle() {
        let mut grid = LightGrid::new();
        grid.turn_on(Position { x: 0, y: 0 }, Position { x: 1, y: 1 });
        
        assert_eq!(grid.lights.get(Position { x: 0, y: 0 }), 'x');
        assert_eq!(grid.lights.get(Position { x: 1, y: 0 }), 'x');
        assert_eq!(grid.lights.get(Position { x: 0, y: 1 }), 'x');
        assert_eq!(grid.lights.get(Position { x: 1, y: 1 }), 'x');
        assert_eq!(grid.lights.count_occurrences('x'), 4);
    }

    #[test]
    fn turn_off_should_turn_off_lights_in_rectangle() {
        let mut grid = LightGrid::new();
        grid.turn_on(Position { x: 0, y: 0 }, Position { x: 999, y: 999 });
        grid.turn_off(Position { x: 0, y: 0 }, Position { x: 1, y: 1 });
        
        assert_eq!(grid.lights.get(Position { x: 0, y: 0 }), 'o');
        assert_eq!(grid.lights.get(Position { x: 1, y: 0 }), 'o');
        assert_eq!(grid.lights.get(Position { x: 0, y: 1 }), 'o');
        assert_eq!(grid.lights.get(Position { x: 1, y: 1 }), 'o');
        assert_eq!(grid.lights.count_occurrences('o'), 4);
    }

    #[test]
    fn toggle_should_toggle_lights_in_rectangle() {
        let mut grid = LightGrid::new();
        grid.toggle(Position { x: 0, y: 0 }, Position { x: 1, y: 1 });
        
        assert_eq!(grid.lights.get(Position { x: 0, y: 0 }), 'x');
        assert_eq!(grid.lights.get(Position { x: 1, y: 0 }), 'x');
        assert_eq!(grid.lights.get(Position { x: 0, y: 1 }), 'x');
        assert_eq!(grid.lights.get(Position { x: 1, y: 1 }), 'x');
        assert_eq!(grid.lights.count_occurrences('x'), 4);

        grid.toggle(Position { x: 0, y: 0 }, Position { x: 0, y: 1 });

        assert_eq!(grid.lights.get(Position { x: 0, y: 0 }), 'o');
        assert_eq!(grid.lights.get(Position { x: 1, y: 0 }), 'x');
        assert_eq!(grid.lights.get(Position { x: 0, y: 1 }), 'o');
        assert_eq!(grid.lights.get(Position { x: 1, y: 1 }), 'x');
        assert_eq!(grid.lights.count_occurrences('x'), 2);
    }
}
