use aoc_core::primitives::Position;

pub fn parse_instructions(input: &str) -> Vec<Instruction> {
    input.lines().map(Instruction::parse).collect()
}

#[derive(Debug, Eq, PartialEq)]
pub enum Instruction {
    TurnOn(Position, Position),
    TurnOff(Position, Position),
    Toggle(Position, Position),
}

impl Instruction {
    pub fn parse(instruction: &str) -> Self {
        if instruction.starts_with("turn on") {
            let (top_left, bottom_right) = Self::parse_positions(&instruction[8..]);
            Self::TurnOn(top_left, bottom_right)
        } else if instruction.starts_with("turn off") {
            let (top_left, bottom_right) = Self::parse_positions(&instruction[9..]);
            Self::TurnOff(top_left, bottom_right)
        } else if instruction.starts_with("toggle") {
            let (top_left, bottom_right) = Self::parse_positions(&instruction[7..]);
            Self::Toggle(top_left, bottom_right)
        } else {
            panic!("Invalid instruction: {}", instruction);
        }
    }

    fn parse_positions(positions: &str) -> (Position, Position) {
        let mut parts = positions.split(" through ");
        let top_left = Self::parse_position(parts.next().unwrap());
        let bottom_right = Self::parse_position(parts.next().unwrap());
        (top_left, bottom_right)
    }

    fn parse_position(position: &str) -> Position {
        let mut parts = position.split(',');
        let x = parts.next().unwrap().parse().unwrap();
        let y = parts.next().unwrap().parse().unwrap();
        Position { x, y }
    }
}

#[cfg(test)]
mod test {
    use aoc_core::primitives::Position;
    use super::Instruction;

    #[test]
    fn parse_should_return_turn_off_instruction() {
        let instruction = Instruction::parse("turn off 660,55 through 986,197");

        assert_eq!(
            instruction,
            Instruction::TurnOff(Position { x: 660, y: 55 }, Position { x: 986, y: 197 })
        );
    }

    #[test]
    fn parse_should_return_turn_on_instruction() {
        let instruction = Instruction::parse("turn on 322,558 through 977,958");

        assert_eq!(
            instruction,
            Instruction::TurnOn(Position { x: 322, y: 558 }, Position { x: 977, y: 958 })
        );
    }

    #[test]
    fn parse_should_return_toggle_instruction() {
        let instruction = Instruction::parse("toggle 322,558 through 977,958");

        assert_eq!(
            instruction,
            Instruction::Toggle(Position { x: 322, y: 558 }, Position { x: 977, y: 958 })
        );
    }
}
