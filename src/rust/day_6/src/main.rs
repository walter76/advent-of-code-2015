mod brightness_grid;
mod instruction;
mod light_grid;

use anyhow::Result;
use instruction::{parse_instructions, Instruction};

fn main() -> Result<()> {
    let puzzle_input = aoc_core::get_input(2015, 6)?;

    let instructions = parse_instructions(&puzzle_input);

    let number_of_lights_on = determine_number_of_lights_on(&instructions);
    println!("Number of lights turned on: {}", number_of_lights_on);

    let total_brightness_level = determine_total_brightness_level(&instructions);
    println!("Total brightness level: {}", total_brightness_level);

    Ok(())
}

fn determine_number_of_lights_on(instructions: &[Instruction]) -> usize {
    let mut grid = light_grid::LightGrid::new();

    for instruction in instructions.iter() {
        match instruction {
            Instruction::TurnOn(top_left, bottom_right) => grid.turn_on(*top_left, *bottom_right),
            Instruction::TurnOff(top_left, bottom_right) => grid.turn_off(*top_left, *bottom_right),
            Instruction::Toggle(top_left, bottom_right) => grid.toggle(*top_left, *bottom_right),
        }
    }

    grid.count_lights_on()
}

fn determine_total_brightness_level(instructions: &[Instruction]) -> i64 {
    let mut grid = brightness_grid::BrightnessGrid::new();

    for instruction in instructions.iter() {
        match instruction {
            Instruction::TurnOn(top_left, bottom_right) => grid.turn_on(*top_left, *bottom_right),
            Instruction::TurnOff(top_left, bottom_right) => grid.turn_off(*top_left, *bottom_right),
            Instruction::Toggle(top_left, bottom_right) => grid.toggle(*top_left, *bottom_right),
        }
    }

    grid.total_brightness()
}
