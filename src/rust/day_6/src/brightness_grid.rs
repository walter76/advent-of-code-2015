use std::cmp::max;

use aoc_core::{int_grid::IntGrid, primitives::Position};

const LIGHT_OFF: i64 = 0;
const INCREASE_BRIGHTNESS: i64 = 1;
const DECREASE_BRIGHTNESS: i64 = -1;
const TOGGLE_BRIGHTNESS: i64 = 2;

const GRID_WIDTH: usize = 1000;
const GRID_HEIGHT: usize = 1000;

pub struct BrightnessGrid {
    brightness: IntGrid,
}

impl BrightnessGrid {
    pub fn new() -> Self {
        Self {
            brightness: IntGrid::new(GRID_WIDTH, GRID_HEIGHT, LIGHT_OFF),
        }
    }

    pub fn turn_on(&mut self, top_left: Position, bottom_right: Position) {
        self.adjust_brightness(top_left, bottom_right, INCREASE_BRIGHTNESS);
    }

    pub fn turn_off(&mut self, top_left: Position, bottom_right: Position) {
        self.adjust_brightness(top_left, bottom_right, DECREASE_BRIGHTNESS);
    }

    pub fn toggle(&mut self, top_left: Position, bottom_right: Position) {
        self.adjust_brightness(top_left, bottom_right, TOGGLE_BRIGHTNESS);
    }

    fn adjust_brightness(&mut self, top_left: Position, bottom_right: Position, amount: i64) {
        for y in top_left.y..=bottom_right.y {
            for x in top_left.x..=bottom_right.x {
                let current_brightness = self.brightness.get(Position { x, y });
                let new_brightness = max(0, current_brightness + amount);
                self.brightness.set(Position { x, y }, new_brightness);
            }
        }
    }

    pub fn total_brightness(&self) -> i64 {
        self.brightness.sum()
    }
}

#[cfg(test)]
mod test {
    use aoc_core::primitives::Position;

    use super::BrightnessGrid;

    #[test]
    fn new_should_create_grid_with_correct_size() {
        let grid = BrightnessGrid::new();
        
        assert_eq!(grid.brightness.width(), 1000);
        assert_eq!(grid.brightness.height(), 1000);
    }

    #[test]
    fn new_should_create_grid_with_all_lights_off() {
        let grid = BrightnessGrid::new();
        
        assert_eq!(grid.brightness.sum(), 0);
    }

    #[test]
    fn turn_on_should_increase_brightness_by_one_in_rectangle() {
        let mut grid = BrightnessGrid::new();
        grid.turn_on(Position { x: 0, y: 0 }, Position { x: 1, y: 1 });
        
        assert_eq!(grid.brightness.get(Position { x: 0, y: 0 }), 1);
        assert_eq!(grid.brightness.get(Position { x: 1, y: 0 }), 1);
        assert_eq!(grid.brightness.get(Position { x: 0, y: 1 }), 1);
        assert_eq!(grid.brightness.get(Position { x: 1, y: 1 }), 1);
        assert_eq!(grid.brightness.sum(), 4);
    }

    #[test]
    fn turn_off_should_decrease_brightness_by_one_in_rectangle() {
        let mut grid = BrightnessGrid::new();
        grid.turn_on(Position { x: 0, y: 0 }, Position { x: 999, y: 999 });
        grid.turn_off(Position { x: 0, y: 0 }, Position { x: 1, y: 1 });
        
        assert_eq!(grid.brightness.get(Position { x: 0, y: 0 }), 0);
        assert_eq!(grid.brightness.get(Position { x: 1, y: 0 }), 0);
        assert_eq!(grid.brightness.get(Position { x: 0, y: 1 }), 0);
        assert_eq!(grid.brightness.get(Position { x: 1, y: 1 }), 0);
        assert_eq!(grid.brightness.sum(), 999996);
    }

    #[test]
    fn toggle_should_increase_brightness_by_two_in_rectangle() {
        let mut grid = BrightnessGrid::new();
        grid.toggle(Position { x: 0, y: 0 }, Position { x: 1, y: 1 });
        
        assert_eq!(grid.brightness.get(Position { x: 0, y: 0 }), 2);
        assert_eq!(grid.brightness.get(Position { x: 1, y: 0 }), 2);
        assert_eq!(grid.brightness.get(Position { x: 0, y: 1 }), 2);
        assert_eq!(grid.brightness.get(Position { x: 1, y: 1 }), 2);
        assert_eq!(grid.brightness.sum(), 8);

        grid.toggle(Position { x: 0, y: 0 }, Position { x: 0, y: 1 });

        assert_eq!(grid.brightness.get(Position { x: 0, y: 0 }), 4);
        assert_eq!(grid.brightness.get(Position { x: 1, y: 0 }), 2);
        assert_eq!(grid.brightness.get(Position { x: 0, y: 1 }), 4);
        assert_eq!(grid.brightness.get(Position { x: 1, y: 1 }), 2);
        assert_eq!(grid.brightness.sum(), 12);
    }
}
