use std::time::Instant;

const NUMBER_OF_ITERATIONS_PART_1: usize = 40;
const NUMBER_OF_ITERATIONS_PART_2: usize = 50;
const INPUT: &str = "1113122113";

fn main() {
    let mut input = String::from(INPUT);

    let start = Instant::now();
    
    for _ in 0 .. NUMBER_OF_ITERATIONS_PART_1 {
        input = look_and_say(input);
    }

    let duration = start.elapsed();

    println!("Part 1 ({} iterations): {} (took {:?})", NUMBER_OF_ITERATIONS_PART_1, input.len(), duration);

    let mut input = String::from(INPUT);

    let start = Instant::now();
    
    for _ in 0 .. NUMBER_OF_ITERATIONS_PART_2 {
        input = look_and_say(input);
    }

    let duration = start.elapsed();

    println!("Part 2 ({} iterations): {}(took {:?})", NUMBER_OF_ITERATIONS_PART_2, input.len(), duration);

    let mut input = String::from(INPUT);

    let start = Instant::now();
    
    for _ in 0 .. NUMBER_OF_ITERATIONS_PART_1 {
        input = look_and_say_2(&input);
    }

    let duration = start.elapsed();

    println!("Part 1 ({} iterations): {} (took {:?})", NUMBER_OF_ITERATIONS_PART_1, input.len(), duration);

    let mut input = String::from(INPUT);

    let start = Instant::now();
    
    for _ in 0 .. NUMBER_OF_ITERATIONS_PART_2 {
        input = look_and_say_2(&input);
    }

    let duration = start.elapsed();

    println!("Part 2 ({} iterations): {}(took {:?})", NUMBER_OF_ITERATIONS_PART_2, input.len(), duration);
}

fn look_and_say(s: String) -> String {
    let mut output = String::new();
    let mut previous_digit: u32 = s.chars().next().unwrap().to_digit(10).unwrap();
    let mut digit_counter: usize = 1;

    if s.len() == 1 {
        return format!("1{}", previous_digit);
    }

    for digit in s.chars().map(|c| c.to_digit(10).unwrap()).skip(1) {
        if digit == previous_digit {
            digit_counter += 1;
        } else {
            output.push_str(&format!("{}{}", digit_counter, previous_digit));
            previous_digit = digit;
            digit_counter = 1;
        }
    }

    output.push_str(&format!("{}{}", digit_counter, previous_digit));

    output
}

fn look_and_say_2(sequence: &str) -> String {
    let mut result = String::new();
    let mut chars = sequence.chars().peekable();

    while let Some(current) = chars.next() {
        let mut count = 1;

        // count consecutive digits
        while chars.peek() == Some(&current) {
            count += 1;
            chars.next();
        }

        // append count and the digit to the result
        result.push_str(&count.to_string());
        result.push(current);
    }

    result
}

#[cfg(test)]
mod tests {
    use crate::look_and_say;

    #[test]
    fn look_and_say_should_create_11_from_1() {
        assert_eq!("11", look_and_say(String::from("1")));
    }

    #[test]
    fn look_and_say_should_create_21_from_11() {
        assert_eq!("21", look_and_say(String::from("11")));
    }

    #[test]
    fn look_and_say_should_create_1211_from_21() {
        assert_eq!("1211", look_and_say(String::from("21")));
    }

    #[test]
    fn look_and_say_should_create_111221_from_1211() {
        assert_eq!("111221", look_and_say(String::from("1211")));
    }

    #[test]
    fn look_and_say_should_create_312211_from_111221() {
        assert_eq!("312211", look_and_say(String::from("111221")));
    }
}
