use std::{collections::HashMap, hash::Hash};

use anyhow::{anyhow, Result};

fn main() -> Result<()> {
    let puzzle_input = aoc_core::get_input(2015, 3)?;

    let number_of_houses_visited = deliver_presents(&puzzle_input)?;
    println!("Number of houses visited: {}", number_of_houses_visited);

    let number_of_houses_visited_with_robo_santa = deliver_presents_with_robo_santa(&puzzle_input)?;
    println!("Number of houses visited with Robo Santa: {}", number_of_houses_visited_with_robo_santa);
    
    Ok(())
}

#[derive(Debug, Eq, PartialEq, Copy, Clone, Hash)]
struct Position(i32, i32);

fn deliver_presents(directions: &str) -> Result<usize> {
    let mut visited: HashMap<Position, usize> = HashMap::new();
    let mut position = Position(0, 0);

    visited.insert(position, 0);

    for direction in directions.chars() {
        move_and_deliver(direction, &mut position, &mut visited)?;
    }

    Ok(visited.len())
}

fn deliver_presents_with_robo_santa(directions: &str) -> Result<usize> {
    let mut visited: HashMap<Position, usize> = HashMap::new();
    let mut santa_position = Position(0, 0);
    let mut robo_santa_position = Position(0, 0);

    visited.insert(santa_position, 0);

    for (i, direction) in directions.chars().enumerate() {
        if i % 2 == 0 {
            move_and_deliver(direction, &mut santa_position, &mut visited)?;
        } else {
            move_and_deliver(direction, &mut robo_santa_position, &mut visited)?;
        }
    }

    Ok(visited.len())
}

fn move_and_deliver(direction: char, position: &mut Position, visited: &mut HashMap<Position, usize>) -> Result<()> {
    match direction {
        '^' => position.1 += 1,
        'v' => position.1 -= 1,
        '>' => position.0 += 1,
        '<' => position.0 -= 1,
        _ => return Err(anyhow!("Invalid character in input: {}", direction)),
    }

    *visited.entry(*position).or_insert(0) += 1;

    Ok(())
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn deliver_presents_example_1_should_return_2() {
        assert_eq!(deliver_presents(">").unwrap(), 2);
    }

    #[test]
    fn deliver_presents_example_2_should_return_4() {
        assert_eq!(deliver_presents("^>v<").unwrap(), 4);
    }

    #[test]
    fn deliver_presents_example_3_should_return_2() {
        assert_eq!(deliver_presents("^v^v^v^v^v").unwrap(), 2);
    }

    #[test]
    fn deliver_presents_with_robo_santa_example_1_should_return_3() {
        assert_eq!(deliver_presents_with_robo_santa("^v").unwrap(), 3);
    }

    #[test]
    fn deliver_presents_with_robo_santa_example_2_should_return_3() {
        assert_eq!(deliver_presents_with_robo_santa("^>v<").unwrap(), 3);
    }

    #[test]
    fn deliver_presents_with_robo_santa_example_3_should_return_11() {
        assert_eq!(deliver_presents_with_robo_santa("^v^v^v^v^v").unwrap(), 11);
    }
}
