use anyhow::Result;

use aoc_core::get_input;

fn main() -> Result<()> {
    let directions = get_input(2015, 1)?;

    println!();
    println!("The directions will lead to floor {}", determine_floor(&directions));

    println!();
    println!(
        "Santa enters the basement at position {}",
        santa_enters_basement_at_position(&directions).unwrap());

    Ok(())
}

fn determine_floor(directions: &str) -> i64 {
    let mut floor = 0;

    for c in directions.chars() {
        match c {
            '(' => floor += 1,
            ')' => floor -= 1,
            _ => (),
        }
    }

    floor
}

fn santa_enters_basement_at_position(directions: &str) -> Option<usize> {
    let mut floor = 0;

    for (index, c) in directions.chars().enumerate() {
        match c {
            '(' => floor += 1,
            ')' => floor -= 1,
            _ => (),
        }

        if floor == -1 {
            return Some(index + 1);
        }
    }

    None
}

#[cfg(test)]
mod tests {
    use crate::{determine_floor, santa_enters_basement_at_position};

    fn create_test_cases() -> Vec<(String, i64)> {
        vec![
            ("(())".to_owned(), 0),
            ("()()".to_owned(), 0),
            ("(((".to_owned(), 3),
            ("(()(()(".to_owned(), 3),
            ("))(((((".to_owned(), 3),
            ("())".to_owned(), -1),
            ("))(".to_owned(), -1),
            (")))".to_owned(), -3),
            (")())())".to_owned(), -3),
        ]
    }

    #[test]
    fn determine_floor_should_return_correct_floor_for_test_data() {
        let test_cases = create_test_cases();

        for (directions, expected_floor) in test_cases.iter() {
            println!("test case: {} -> {}", directions, expected_floor);

            assert_eq!(*expected_floor, determine_floor(&directions));
        }
    }

    /// `)` causes him to enter the basement at character position 1.
    #[test]
    fn santa_enters_basement_at_position_should_return_1_for_example_1() {
        assert_eq!(Some(1), santa_enters_basement_at_position(")"));
    }

    /// `()())` causes him to enter the basement at character position 5.
    #[test]
    fn santa_enters_basement_at_position_should_return_5_for_example_2() {
        assert_eq!(Some(5), santa_enters_basement_at_position("()())"));
    }

    #[test]
    fn santa_enters_basement_at_position_should_return_none_if_he_never_enters_basement() {
        assert_eq!(None, santa_enters_basement_at_position("("));
    }
}

