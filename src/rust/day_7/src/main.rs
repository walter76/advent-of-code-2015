mod instruction;

use std::collections::HashMap;
use anyhow::Result;

use instruction::{Instruction, Operand, WireIdentifier};

fn main() -> Result<()>{
    let puzzle_input = aoc_core::get_input(2015, 7)?;

    let instructions = puzzle_input.lines().map(Instruction::parse).collect::<Vec<Instruction>>();
    let mut logic_gates: HashMap<WireIdentifier, Instruction> = HashMap::new();

    for instruction in instructions.iter() {
        match instruction {
            Instruction::ProvideSignal(_, wire_id) => {
                logic_gates.insert(wire_id.clone(), instruction.clone());
            }
            Instruction::Not(_, wire_id) => {
                logic_gates.insert(wire_id.clone(), instruction.clone());
            }
            Instruction::And(_, _, wire_id) => {
                logic_gates.insert(wire_id.clone(), instruction.clone());
            }
            Instruction::Or(_, _, wire_id) => {
                logic_gates.insert(wire_id.clone(), instruction.clone());
            }
            Instruction::LShift(_, _, wire_id) => {
                logic_gates.insert(wire_id.clone(), instruction.clone());
            }
            Instruction::RShift(_, _, wire_id) => {
                logic_gates.insert(wire_id.clone(), instruction.clone());
            }
        }
    }

    let mut wires: HashMap<WireIdentifier, i32> = HashMap::new();
    let wire_a = WireIdentifier("a".to_string());
    let result = execute_instruction(&wire_a, &logic_gates, &mut wires);

    println!("The value of wire a is: {}", result);

    wires.clear();
    wires.insert(WireIdentifier("b".to_string()), result);

    let result = execute_instruction(&wire_a, &logic_gates, &mut wires);

    println!("The value of wire a after override and reset is: {}", result);

    Ok(())
}

// This works under the assumption that a wire is only evaluated once. In this case,
// if the wire is not found in the wires HashMap, we can look up the instruction that
// evaluates the wire and execute it.

fn execute_instruction(wire_to_evaluate: &WireIdentifier, logic_gates: &HashMap<WireIdentifier, Instruction>, wires: &mut HashMap<WireIdentifier, i32>) -> i32 {
    // we already know the value of the wire
    if let Some(value) = wires.get(wire_to_evaluate) {
        return *value;
    }

    // we have to calculate the value of the wire
    let instruction = logic_gates.get(wire_to_evaluate).unwrap();

    match instruction {
        Instruction::ProvideSignal(operand1, target_wire_id) => {
            match operand1 {
                Operand::Value(value) => {
                    wires.insert(target_wire_id.clone(), *value);
                    return *value;
                }
                Operand::Wire(source_wire_id) => {
                    if let Some(&value) = wires.get(source_wire_id) {
                        wires.insert(target_wire_id.clone(), value);
                        return value;
                    } else {
                        return execute_instruction(source_wire_id, logic_gates, wires);
                    }
                }
            }
        }
        Instruction::Not(source_wire_id, target_wire_id) => {
            if let Some(&value) = wires.get(source_wire_id) {
                let result = !value;
                wires.insert(target_wire_id.clone(), result);
                return result;
            } else {
                let value = execute_instruction(source_wire_id, logic_gates, wires);
                let result = !value;
                wires.insert(target_wire_id.clone(), result);
                return result;
            }
        }
        Instruction::And(operand1, operand2, target_wire_id) => {
            let value1 = match operand1 {
                Operand::Value(value) => *value,
                Operand::Wire(source_wire_id) => {
                    if let Some(&value) = wires.get(source_wire_id) {
                        value
                    } else {
                        execute_instruction(source_wire_id, logic_gates, wires)
                    }
                }
            };

            let value2 = match operand2 {
                Operand::Value(value) => *value,
                Operand::Wire(source_wire_id) => {
                    if let Some(&value) = wires.get(source_wire_id) {
                        value
                    } else {
                        execute_instruction(source_wire_id, logic_gates, wires)
                    }
                }
            };

            let result = value1 & value2;
            wires.insert(target_wire_id.clone(), result);
            return result;
        }
        Instruction::Or(operand1, operand2, target_wire_id) => {
            let value1 = match operand1 {
                Operand::Value(value) => *value,
                Operand::Wire(source_wire_id) => {
                    if let Some(&value) = wires.get(source_wire_id) {
                        value
                    } else {
                        execute_instruction(source_wire_id, logic_gates, wires)
                    }
                }
            };

            let value2 = match operand2 {
                Operand::Value(value) => *value,
                Operand::Wire(source_wire_id) => {
                    if let Some(&value) = wires.get(source_wire_id) {
                        value
                    } else {
                        execute_instruction(source_wire_id, logic_gates, wires)
                    }
                }
            };

            let result = value1 | value2;
            wires.insert(target_wire_id.clone(), result);
            return result;
        }
        Instruction::LShift(source_wire_id, shift_amount, target_wire_id) => {
            let value = if let Some(&value) = wires.get(source_wire_id) {
                    value
                } else {
                    execute_instruction(source_wire_id, logic_gates, wires)
                };

            let result = value << shift_amount;
            wires.insert(target_wire_id.clone(), result);
            return result;
        }
        Instruction::RShift(source_wire_id, shift_amount, target_wire_id) => {
            let value = if let Some(&value) = wires.get(source_wire_id) {
                value
            } else {
                execute_instruction(source_wire_id, logic_gates, wires)
            };

            let result = value >> shift_amount;
            wires.insert(target_wire_id.clone(), result);
            return result;
        }
    }
}