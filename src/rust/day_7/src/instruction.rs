#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct WireIdentifier(pub String);

#[derive(Debug, Clone, Eq, PartialEq)]
pub enum Operand {
    Wire(WireIdentifier),
    Value(i32),
}

impl Operand {
    pub fn parse(operand: &str) -> Self {
        if operand.chars().all(char::is_numeric) {
            Self::Value(operand.parse().unwrap())
        } else {
            Self::Wire(WireIdentifier(operand.to_string()))
        }
    }
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub enum Instruction {
    ProvideSignal(Operand, WireIdentifier),
    Not(WireIdentifier, WireIdentifier),
    And(Operand, Operand, WireIdentifier),
    Or(Operand, Operand, WireIdentifier),
    LShift(WireIdentifier, usize, WireIdentifier),
    RShift(WireIdentifier, usize, WireIdentifier),
}

impl Instruction {
    pub fn parse(instruction: &str) -> Self {
        let parts: Vec<&str> = instruction.split_whitespace().collect();
    
        if parts.len() == 3 {
            Self::ProvideSignal(Operand::parse(parts[0]), WireIdentifier(parts[2].to_string()))
        } else if parts.len() == 4 {
            Self::Not(WireIdentifier(parts[1].to_string()), WireIdentifier(parts[3].to_string()))
        } else {
            match parts[1] {
                "AND" => return Self::And(Operand::parse(parts[0]), Operand::parse(parts[2]), WireIdentifier(parts[4].to_string())),
                "OR" => return Self::Or(Operand::parse(parts[0]), Operand::parse(parts[2]), WireIdentifier(parts[4].to_string())),
                "LSHIFT" => return Self::LShift(WireIdentifier(parts[0].to_string()), parts[2].parse().unwrap(), WireIdentifier(parts[4].to_string())),
                "RSHIFT" => return Self::RShift(WireIdentifier(parts[0].to_string()), parts[2].parse().unwrap(), WireIdentifier(parts[4].to_string())),
                _ => panic!("Unknown instruction: {}", instruction),
            }
        }
    }
}

#[cfg(test)]
mod test {
    use crate::instruction::WireIdentifier;

    use super::{Instruction, Operand};

    #[test]
    fn parse_operand_should_parse_value() {
        assert_eq!(Operand::parse("123"), Operand::Value(123));
    }

    #[test]
    fn parse_operand_should_parse_wire() {
        assert_eq!(Operand::parse("x"), Operand::Wire(WireIdentifier("x".to_string())));
    }
    
    #[test]
    fn parse_instruction_should_parse_provide_signal_with_value() {
        assert_eq!(Instruction::parse("123 -> x"),
            Instruction::ProvideSignal(
                Operand::Value(123),
                WireIdentifier("x".to_string())));
    }

    #[test]
    fn parse_instruction_should_parse_provide_signal_with_wire() {
        assert_eq!(Instruction::parse("y -> x"),
            Instruction::ProvideSignal(
                Operand::Wire(WireIdentifier("y".to_string())),
                WireIdentifier("x".to_string())));
    }

    #[test]
    fn parse_instruction_should_parse_not() {
        assert_eq!(Instruction::parse("NOT x -> h"),
            Instruction::Not(
                WireIdentifier("x".to_string()),
                WireIdentifier("h".to_string())));
    }

    #[test]
    fn parse_instruction_should_parse_and() {
        assert_eq!(Instruction::parse("x AND y -> d"),
            Instruction::And(
                Operand::Wire(WireIdentifier("x".to_string())),
                Operand::Wire(WireIdentifier("y".to_string())),
                WireIdentifier("d".to_string())));
    }

    #[test]
    fn parse_instruction_should_parse_or() {
        assert_eq!(Instruction::parse("x OR y -> e"),
            Instruction::Or(
                Operand::Wire(WireIdentifier("x".to_string())),
                Operand::Wire(WireIdentifier("y".to_string())),
                WireIdentifier("e".to_string())));
    }

    #[test]
    fn parse_instruction_should_parse_lshift() {
        assert_eq!(Instruction::parse("x LSHIFT 2 -> f"),
            Instruction::LShift(
                WireIdentifier("x".to_string()),
                2,
                WireIdentifier("f".to_string())));
    }

    #[test]
    fn parse_instruction_should_parse_rshift() {
        assert_eq!(Instruction::parse("y RSHIFT 2 -> g"),
            Instruction::RShift(
                WireIdentifier("y".to_string()),
                2,
                WireIdentifier("g".to_string())));
    }
}
