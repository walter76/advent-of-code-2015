use anyhow::Result;

fn main() -> Result<()> {
    let puzzle_input = aoc_core::get_input(2015, 8)?;

    let result = calculate_space_difference(&puzzle_input);
    println!("Result: {}", result);

    let result_encoded = calculate_space_difference_encoded(&puzzle_input);
    println!("Result (encoded): {}", result_encoded);
    
    Ok(())
}

fn calculate_space_difference(input: &str) -> usize {
    let mut total_chars_of_string_literals = 0;
    let mut total_chars_of_in_memory_representations = 0;

    for (i, line) in input.lines().enumerate() {
        let string_literal = count_chars_of_string_literal(line);
        total_chars_of_string_literals += string_literal;

        let in_memory_representation = count_chars_of_in_memory_representation(line);
        total_chars_of_in_memory_representations += in_memory_representation;

        println!("Line {}: '{}' (literal={}, in_memory={})", i, line, string_literal, in_memory_representation);
    }

    println!("Total chars of string literals: {}", total_chars_of_string_literals);
    println!("Total chars of in-memory representations: {}", total_chars_of_in_memory_representations);

    total_chars_of_string_literals - total_chars_of_in_memory_representations
}

fn count_chars_of_string_literal(s: &str) -> usize {
    s.len()
}

fn count_chars_of_in_memory_representation(s: &str) -> usize {
    let normalized = &s[1..s.len() - 1];

    let mut number_of_chars = 0;
    let mut index = 0;

    if index >= normalized.len() {
        return 0;
    }

    loop {
        let c = normalized.chars().nth(index).unwrap();

        if c == '\\' {
            let next_c = normalized.chars().nth(index + 1).unwrap();

            if next_c == 'x' {
                index += 4;
            } else {
                index += 2;
            }
        } else {
            index += 1;
        }

        number_of_chars += 1;

        if index >= normalized.len() {
            break;
        }
    }

    number_of_chars
}

fn calculate_space_difference_encoded(input: &str) -> usize {
    let mut total_chars_of_encoded_string_literals = 0;
    let mut total_chars_of_string_literals = 0;

    for (i, line) in input.lines().enumerate() {
        let encoded = encode_string_literal(line);
        total_chars_of_encoded_string_literals += encoded.len();

        let string_literal = count_chars_of_string_literal(line);
        total_chars_of_string_literals += string_literal;
    }

    total_chars_of_encoded_string_literals - total_chars_of_string_literals
}

fn encode_string_literal(s: &str) -> String {
    let mut encoded = String::new();

    encoded.push('"');

    for c in s.chars() {
        match c {
            '\\' => encoded.push_str("\\\\"),
            '"' => encoded.push_str("\\\""),
            _ => encoded.push(c),
        }
    }

    encoded.push('"');

    encoded
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn count_chars_of_string_literal_should_return_2_for_empty_literal() {
        assert_eq!(count_chars_of_string_literal("\"\""), 2);
    }

    #[test]
    fn count_chars_of_string_literal_should_return_5_for_literal_with_3_chars() {
        assert_eq!(count_chars_of_string_literal("\"abc\""), 5);
    }

    #[test]
    fn count_chars_of_string_literal_should_return_10_for_literal_with_7_chars() {
        assert_eq!(count_chars_of_string_literal("\"aaa\\\"aaa\""), 10);
    }

    #[test]
    fn count_chars_of_string_literal_should_return_6_for_literal_with_4_chars() {
        assert_eq!(count_chars_of_string_literal("\"\\x27\""), 6);
    }

    #[test]
    fn count_chars_of_in_memory_representation_should_return_0_for_empty_literal() {
        assert_eq!(count_chars_of_in_memory_representation("\"\""), 0);
    }

    #[test]
    fn count_chars_of_in_memory_representation_should_return_3_for_literal_with_3_chars() {
        assert_eq!(count_chars_of_in_memory_representation("\"abc\""), 3);
    }

    #[test]
    fn count_chars_of_in_memory_representation_should_return_7_for_literal_with_7_chars() {
        assert_eq!(count_chars_of_in_memory_representation("\"aaa\\\"aaa\""), 7);
    }

    #[test]
    fn count_chars_of_in_memory_representation_should_return_1_for_literal_with_4_chars() {
        assert_eq!(count_chars_of_in_memory_representation("\"\\x27\""), 1);
    }

    #[test]
    fn count_chars_of_in_memory_representation_should_return_3_for_literal_with_6_chars() {
        assert_eq!(count_chars_of_in_memory_representation("\"a\\x27a\""), 3);
    }

    #[test]
    fn encode_string_literal_should_return_4_chars_for_empty_literal() {
        assert_eq!(encode_string_literal("\"\""), "\"\\\"\\\"\"");
    }

    #[test]
    fn encode_string_literal_should_return_9_chars_for_literal_with_5_chars() {
        assert_eq!(encode_string_literal("\"abc\""), "\"\\\"abc\\\"\"");
    }

    #[test]
    fn encode_string_literal_should_return_16_chars_for_literal_with_10_chars() {
        assert_eq!(encode_string_literal("\"aaa\\\"aaa\""), "\"\\\"aaa\\\\\\\"aaa\\\"\"");
    }

    #[test]
    fn encode_string_literal_should_return_6_chars_for_literal_with_4_chars() {
        assert_eq!(encode_string_literal("\"\\x27\""), "\"\\\"\\\\x27\\\"\"");
    }
}
