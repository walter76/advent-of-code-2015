// TODO: Maximum length of password is 8 characters!

use std::collections::HashSet;

const INPUT_PASSWORD: &str = "hxbxwxba";

fn main() {
    let next_valid_password = find_next_valid_password(INPUT_PASSWORD);

    println!(
        "The next valid password after {} is: {}",
        INPUT_PASSWORD, next_valid_password);

    let next_next_valid_password = find_next_valid_password(&next_valid_password);

    println!(
        "An the next next valid password after {} is: {}",
        next_valid_password, next_next_valid_password);
}

fn find_next_valid_password(input_password: &str) -> String {
    let mut next_password = inc_password(input_password);

    while !is_valid_password(&next_password) {
        next_password = inc_password(&next_password);
    }

    next_password
}

fn inc_password(password: &str) -> String {
    let mut new_password = String::new();
    let mut carry = true;

    for c in password.chars().rev() {
        if carry {
            if c == 'z' {
                new_password.push('a');
            } else {
                new_password.push((c as u8 + 1) as char);
                carry = false;
            }
        } else {
            new_password.push(c);
        }
    }

    if carry {
        new_password.push('a');
    }

    new_password.chars().rev().collect()
}

const FORBIDDEN_CHARS: &str = "iol";

fn is_valid_password(password: &str) -> bool {
    !has_forbidden_chars(password)
    && has_increasing_straights_of_three(password)
    && has_at_least_two_different_doubles(password)
}

fn has_increasing_straights_of_three(password: &str) -> bool {
    for w in password.chars().collect::<Vec<_>>().windows(3) {
        if w[0] as u8 + 1 == w[1] as u8 && w[1] as u8 + 1 == w[2] as u8 {
            return true;
        }
    }

    false
}

fn has_forbidden_chars(password: &str) -> bool {
    let contains_forbidden_chars =
        password.chars().any(|c| FORBIDDEN_CHARS.contains(c));

    contains_forbidden_chars 
}

fn has_at_least_two_different_doubles(password: &str) -> bool {
    let mut it = password.chars().peekable();
    let mut non_overlapping_pairs: Vec<String> = vec![];

    while let Some(c) = it.next() {
        if let Some(next_c) = it.peek() {
            if c == *next_c {
                // found some pair
                non_overlapping_pairs.push(format!("{}{}", c, next_c));
                it.next();
            }
        }
    }

    remove_duplicates(non_overlapping_pairs).iter().count() > 1
}

fn remove_duplicates(pairs: Vec<String>) -> Vec<String> {
    let set: HashSet<_> = pairs.into_iter().collect();
    set.into_iter().collect()
}

#[cfg(test)]
mod tests {
    use crate::{has_at_least_two_different_doubles, has_forbidden_chars, has_increasing_straights_of_three, inc_password, is_valid_password};

    #[test]
    fn inc_password_should_create_b_from_a() {
        assert_eq!("b", inc_password("a"));
    }

    #[test]
    fn inc_password_should_create_z_from_y() {
        assert_eq!("z", inc_password("y"));
    }

    #[test]
    fn inc_password_should_create_aa_from_z() {
        assert_eq!("aa", inc_password("z"));
    }

    #[test]
    fn inc_password_should_create_ab_from_aa() {
        assert_eq!("ab", inc_password("aa"));
    }

    #[test]
    fn inc_password_should_create_ba_from_az() {
        assert_eq!("ba", inc_password("az"));
    }

    #[test]
    fn inc_password_should_create_aaa_from_zz() {
        assert_eq!("aaa", inc_password("zz"));
    }

    #[test]
    fn inc_password_should_create_baa_from_azz() {
        assert_eq!("baa", inc_password("azz"));
    }

    #[test]
    fn has_forbidden_chars_should_return_true_for_password_containing_i() {
        assert!(has_forbidden_chars("iab"));
    }

    #[test]
    fn has_forbidden_chars_should_return_true_for_password_containing_o() {
        assert!(has_forbidden_chars("aob"));
    }

    #[test]
    fn has_forbidden_chars_should_return_true_for_password_containing_l() {
        assert!(has_forbidden_chars("abl"));
    }

    #[test]
    fn has_forbidden_chars_should_return_false_for_password_not_containing_iol() {
        assert!(!has_forbidden_chars("abc"));
    }

    #[test]
    fn is_valid_password_should_return_false_for_password_containing_iol() {
        assert!(!is_valid_password("hijklmmn"));
    }

    #[test]
    fn has_increasing_straights_of_three_should_return_true_for_abc() {
        assert!(has_increasing_straights_of_three("abc"));
    }

    #[test]
    fn has_increasing_straights_of_three_should_return_false_for_abd() {
        assert!(!has_increasing_straights_of_three("abd"));
    }

    #[test]
    fn has_increasing_straights_of_three_should_return_true_for_hijklmmn() {
        assert!(has_increasing_straights_of_three("hijklmmn"));
    }

    #[test]
    fn has_increasing_straights_of_three_should_return_true_for_abbcdgjk() {
        assert!(has_increasing_straights_of_three("abbcdgjk"));
    }

    #[test]
    fn has_at_least_two_different_doubles_should_return_false_for_abcd() {
        assert!(!has_at_least_two_different_doubles("abcd"));
    }

    #[test]
    fn has_at_least_two_different_doubles_should_return_false_for_aacd() {
        assert!(!has_at_least_two_different_doubles("aacd"));
    }

    #[test]
    fn has_at_least_two_different_doubles_should_return_false_for_aaa() {
        assert!(!has_at_least_two_different_doubles("aaa"));
    }

    #[test]
    fn has_at_least_two_different_doubles_should_return_false_for_aaaa() {
        assert!(!has_at_least_two_different_doubles("aaaa"));
    }

    #[test]
    fn has_at_least_two_different_doubles_should_return_true_for_aabb() {
        assert!(has_at_least_two_different_doubles("aabb"));
    }

    #[test]
    fn has_at_least_two_different_doubles_should_return_true_for_aabbcc() {
        assert!(has_at_least_two_different_doubles("aabbcc"));
    }

    #[test]
    fn is_valid_password_should_return_false_for_hijklmmn() {
        assert!(!is_valid_password("hijklmmn"));
    }

    #[test]
    fn is_valid_password_should_return_false_for_abbceffg() {
        assert!(!is_valid_password("abbceffg"));
    }

    #[test]
    fn is_valid_password_should_return_false_for_abbcegjk() {
        assert!(!is_valid_password("abbcegjk"));
    }
}