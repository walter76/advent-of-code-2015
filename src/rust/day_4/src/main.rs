fn main() {
    const PUZZLE_INPUT: &str = "ckczppom";

    let advent_coin = mine_advent_coin(PUZZLE_INPUT);
    println!("Advent coin: {}", advent_coin);

    let advent_coin_6_zeros = mine_advent_coin_6_zeros(PUZZLE_INPUT);
    println!("Advent coin with 6 zeros: {}", advent_coin_6_zeros);
}

fn mine_advent_coin(secret_key: &str) -> u64 {
    let mut i = 0;

    loop {
        let input = format!("{}{}", secret_key, i);
        let digest = md5::compute(input);

        // Indexing on `Digest` returns the byte at that index. If
        // we want to check if the first 5 digets are 0, we can
        // do so by checking if the first two bytes are 0 and the
        // third byte is less than 0x0F.
        if digest[0] == 0 && digest[1] == 0 && digest[2] <= 0x0F {
            return i;
        }

        i += 1;
    }
}

fn mine_advent_coin_6_zeros(secret_key: &str) -> u64 {
    let mut i = 0;

    loop {
        let input = format!("{}{}", secret_key, i);
        let digest = md5::compute(input);

        // Indexing on `Digest` returns the byte at that index. If
        // we want to check if the first 6 digets are 0, we can
        // do so by checking if the first three bytes are 0.
        if digest[0] == 0 && digest[1] == 0 && digest[2] == 0 {
            return i;
        }

        i += 1;
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn mine_advent_coin_should_return_609043_for_abcdef() {
        assert_eq!(mine_advent_coin("abcdef"), 609043);
    }

    #[test]
    fn mine_advent_coin_should_return_1048970_for_pqrstuv() {
        assert_eq!(mine_advent_coin("pqrstuv"), 1048970);
    }
}
