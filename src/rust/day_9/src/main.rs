use anyhow::Result;
use aoc_core::undirected_graph::UndirectedGraph;

// At first I thought, that Dijkstra's algorithm will be the solution, but this
// cannot work, as Dijkstra will find the shortest path between two nodes and
// not visit every node. The task was, to visit every node and find the shortest/
// longest path. Therefore a DFS and BFS are the correct algorithms to use.

fn main() -> Result<()> {
    // let graph = create_graph();
    let graph = create_graph_from_puzzle_input()?;

    println!("{:?}", graph);
    
    let mut shortest_distance = i32::MAX;
    let mut shortest_path = vec![];

    for node in graph.get_nodes().iter() {
        let (path, total_distance) = graph.find_shortest_path(node);

        if total_distance < shortest_distance {
            shortest_distance = total_distance;
            shortest_path = path;
        }
    }

    println!("Shortest path and distance: {:?} -> {}", shortest_path, shortest_distance);

    let mut longest_distance = 0;
    let mut longest_path = vec![];

    for node in graph.get_nodes().iter() {
        let (path, total_distance) = graph.longest_path(node);

        if total_distance > longest_distance {
            longest_distance = total_distance;
            longest_path = path;
        }
    }

    println!("Longest path and distance: {:?} -> {}", longest_path, longest_distance);

    Ok(())
}

fn create_graph() -> UndirectedGraph {
    let mut graph = UndirectedGraph::new();

    graph.add_node("London");
    
    graph.add_node("Dublin");
    graph.add_edge("London", "Dublin", 464);

    graph.add_node("Belfast");
    graph.add_edge("London", "Belfast", 518);

    graph.add_edge("Dublin", "Belfast", 141);

    graph
}

fn create_graph_from_puzzle_input() -> Result<UndirectedGraph> {
    let puzzle_input = aoc_core::get_input(2015, 9)?;

    let mut graph = UndirectedGraph::new();

    for line in puzzle_input.lines() {
        let parts: Vec<&str> = line.split(" = ").collect();
        let cities: Vec<&str> = parts[0].split(" to ").collect();

        graph.add_node(cities[0]);
        graph.add_node(cities[1]);

        graph.add_edge(cities[0], cities[1], parts[1].parse()?);
    }

    Ok(graph)
}
