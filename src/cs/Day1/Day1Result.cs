using System.Collections.Generic;
using NUnit.Framework;

using AocCore;

namespace Day1
{
    public record Day1Result(int Floor, int SantaEntersBasementAtPosition);

    internal class Day1ResultCalculator
    {
        public static Day1Result Calculate()
        {
            var year = 2015;
            var day = 1;
            var directions = PuzzleInput.GetInput(year, day);

            return new Day1Result(DetermineFloor(directions), SantaEntersBasementAtPosition(directions));
        }

        internal static int DetermineFloor(string directions)
        {
            var floor = 0;

            foreach (var c in directions)
            {
                if (c == '(')
                {
                    floor += 1;
                }
                else if (c == ')')
                {
                    floor -= 1;
                }
            }

            return floor;
        }

        internal static int SantaEntersBasementAtPosition(string directions)
        {
            var floor = 0;

            for (int index = 0; index < directions.Length; index++)
            {
                if (directions[index] == '(')
                {
                    floor += 1;
                }
                else if (directions[index] == ')')
                {
                    floor -= 1;
                }

                if (floor == -1)
                {
                    return index + 1;
                }
            }

            return 0;
        }
    }
}

namespace Day1.Tests
{
    [TestFixture]
    public class Day1ResultCalculatorTests
    {
        private IReadOnlyCollection<(string, int)> TestCases()
        {
            var testCases = new List<(string, int)>
            {
                ("(())", 0),
                ("()()", 0),
                ("(((", 3),
                ("(()(()(", 3),
                ("))(((((", 3),
                ("())", -1),
                ("))(", -1),
                (")))", -3),
                (")())())", -3)
            };

            return testCases.AsReadOnly();
        }

        [Test]
        public void DetermineFloorShouldReturnCorrectFloorForTestCases()
        {
            var testCases = TestCases();

            foreach (var testCase in testCases)
            {
                Assert.That(Day1ResultCalculator.DetermineFloor(testCase.Item1), Is.EqualTo(testCase.Item2));
            }
        }

        [Test]
        public void SantaEntersBasementAtPositionShouldReturn1ForExample1() {
            Assert.That(Day1ResultCalculator.SantaEntersBasementAtPosition(")"), Is.EqualTo(1));
        }

        [Test]
        public void SantaEntersBasementAtPositionShouldReturn5ForExample2() {
            Assert.That(Day1ResultCalculator.SantaEntersBasementAtPosition("()())"), Is.EqualTo(5));
        }
    }
}
