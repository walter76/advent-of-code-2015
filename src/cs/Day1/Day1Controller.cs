using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Day1
{
    [ApiController]
    [Route("api/day1")]
    public class Day1Controller : ControllerBase
    {
        private readonly ILogger<Day1Controller> _logger;

        public Day1Controller(ILogger<Day1Controller> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public Day1Result Get()
        {
            _logger.LogInformation("Calculating results for day 1.");

            return Day1ResultCalculator.Calculate();
        }
    }
}
