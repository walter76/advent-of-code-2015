using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Day2
{
    [ApiController]
    [Route("api/day2")]
    public class Day2Controller : ControllerBase
    {
        private readonly ILogger<Day2Controller> _logger;

        public Day2Controller(ILogger<Day2Controller> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public Day2Result Get()
        {
            _logger.LogInformation("Calculating results for day 2.");

            return Day2ResultCalculator.Calculate();
        }
    }
}
