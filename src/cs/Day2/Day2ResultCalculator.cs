using NUnit.Framework;

using AocCore;
using System;
using System.Linq;

namespace Day2
{
    public record Day2Result(uint TotalPaperNeeded, uint TotalRibbonNeeded);

    internal class Day2ResultCalculator
    {
        public static Day2Result Calculate()
        {
            var year = 2015;
            var day = 2;
            var puzzleInput = PuzzleInput.GetInputTrimmedAndSplitByLine(year, day);
            var dimensions = puzzleInput.Select(d => new Dimensions(d));

            uint totalPaperNeeded = 0;
            foreach (var d in dimensions)
            {
                totalPaperNeeded += Day2ResultCalculator.CalculatePaperNeeded(d);
            }

            uint totalRibbonNeeded = 0;
            foreach (var d in dimensions)
            {
                totalRibbonNeeded += Day2ResultCalculator.CalculateRibbonNeeded(d);
            }

            return new Day2Result(totalPaperNeeded, totalRibbonNeeded);
        }

        internal static uint CalculatePaperNeeded(Dimensions dimensions)
        {
            var surfaceArea = dimensions.SurfaceArea();
            var smallestSideArea = dimensions.SmallestSideArea();

            return surfaceArea + smallestSideArea;
        }

        internal static uint CalculateRibbonNeeded(Dimensions dimensions)
        {
            var ribbonLengthForWrapping = dimensions.SmallestPerimeterSide();
            var ribbonLengthForBow = dimensions.CubicLength();

            return ribbonLengthForWrapping + ribbonLengthForBow;
        }
    }

    internal class Dimensions
    {
        public uint Width { get; set; }
        public uint Height { get; set; }
        public uint Length { get; set; }

        public Dimensions(string s)
        {
            var parts = s.Split('x');

            Width = uint.Parse(parts[1]);
            Height = uint.Parse(parts[2]);
            Length = uint.Parse(parts[0]);
        }

        public uint SurfaceArea()
        {
            var side1 = Width * Height;
            var side2 = Width * Length;
            var side3 = Height * Length;

            return 2 * side1 + 2 * side2 + 2 * side3;
        }

        public uint SmallestSideArea()
        {
            var side1 = Width * Height;
            var side2 = Width * Length;
            var side3 = Height * Length;

            return uint.Min(uint.Min(side1, side2), side3);
        }

        public uint SmallestPerimeterSide()
        {
            var left_face = Height * 2 + Length * 2;
            var top_face = Width * 2 + Length * 2;
            var front_face = Height * 2 + Width * 2;

            return uint.Min(uint.Min(left_face, top_face), front_face);
        }

        public uint CubicLength()
        {
            return Width * Height * Length;
        }
    }
}

namespace Day2.Tests
{
    [TestFixture]
    public class Day2ResultCalculatorTests
    {
        [Test]
        public void CalculatePaperNeededShouldReturn58For2x3x4()
        {
            var dimensions = new Dimensions("2x3x4");

            Assert.That(Day2ResultCalculator.CalculatePaperNeeded(dimensions), Is.EqualTo(58));
        }

        [Test]
        public void CalculatePaperNeededShouldReturn43For1x1x10()
        {
            var dimensions = new Dimensions("1x1x10");

            Assert.That(Day2ResultCalculator.CalculatePaperNeeded(dimensions), Is.EqualTo(43));
        }

        [Test]
        public void CalculateRibbonNeededShouldReturn34For2x3x4()
        {
            var dimensions = new Dimensions("2x3x4");

            Assert.That(Day2ResultCalculator.CalculateRibbonNeeded(dimensions), Is.EqualTo(34));
        }

        [Test]
        public void CalculateRibbonNeededShouldReturn14For1x1x10()
        {
            var dimensions = new Dimensions("1x1x10");

            Assert.That(Day2ResultCalculator.CalculateRibbonNeeded(dimensions), Is.EqualTo(14));
        }
    }

    [TestFixture]
    public class DimensionsTests
    {
        [Test]
        public void DimensionsShouldReturn2lx3wx4hFor2x3x4()
        {
            var dimensions = new Dimensions("2x3x4");

            Assert.That(dimensions.Length, Is.EqualTo(2));
            Assert.That(dimensions.Width, Is.EqualTo(3));
            Assert.That(dimensions.Height, Is.EqualTo(4));
        }

        [Test]
        public void DimensionsShouldReturn1lx1wx10hFor1x1x10()
        {
            var dimensions = new Dimensions("1x1x10");

            Assert.That(dimensions.Length, Is.EqualTo(1));
            Assert.That(dimensions.Width, Is.EqualTo(1));
            Assert.That(dimensions.Height, Is.EqualTo(10));
        }

        [Test]
        public void SurfaceAreaShouldReturn52For2x3x4()
        {
            var dimensions = new Dimensions("2x3x4");

            Assert.That(dimensions.SurfaceArea(), Is.EqualTo(52));
        }

        [Test]
        public void SurfaceAreaShouldReturn42For1x1x10()
        {
            var dimensions = new Dimensions("1x1x10");

            Assert.That(dimensions.SurfaceArea(), Is.EqualTo(42));
        }

        [Test]
        public void SmallestSideAreaShouldReturn6For2x3x4()
        {
            var dimensions = new Dimensions("2x3x4");

            Assert.That(dimensions.SmallestSideArea(), Is.EqualTo(6));
        }

        [Test]
        public void SmallestSideAreaShouldReturn1For1x1x10()
        {
            var dimensions = new Dimensions("1x1x10");

            Assert.That(dimensions.SmallestSideArea(), Is.EqualTo(1));
        }

        [Test]
        public void SmallestPerimeterSideShouldReturn10For2x3x4()
        {
            var dimensions = new Dimensions("2x3x4");

            Assert.That(dimensions.SmallestPerimeterSide(), Is.EqualTo(10));
        }

        [Test]
        public void SmallestPerimeterSideShouldReturn4For1x1x10()
        {
            var dimensions = new Dimensions("1x1x10");

            Assert.That(dimensions.SmallestPerimeterSide(), Is.EqualTo(4));
        }

        [Test]
        public void CubicLengthShouldReturn24For2x3x4()
        {
            var dimensions = new Dimensions("2x3x4");

            Assert.That(dimensions.CubicLength(), Is.EqualTo(24));
        }

        [Test]
        public void CubicLengthShouldReturn10For1x1x10()
        {
            var dimensions = new Dimensions("1x1x10");

            Assert.That(dimensions.CubicLength(), Is.EqualTo(10));
        }
    }
}
