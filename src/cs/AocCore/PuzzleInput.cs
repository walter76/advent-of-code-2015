using System;
using System.IO;
using System.Net.Http;

namespace AocCore
{
    public class PuzzleInput
    {
        public static string[] GetInputTrimmedAndSplitByLine(int year, int day)
        {
            var puzzleInput = PuzzleInput.GetInput(year, day);

            return puzzleInput
                .Trim()
                .Split(new[] { "\r\n", "\n" }, StringSplitOptions.RemoveEmptyEntries);        
        }

        public static string GetInput(int year, int day)
        {
            var dataDirectory = PuzzleInput.ProvisionDataDirectory(year, day);
            string puzzleInputPath = Path.Combine(dataDirectory, "input.txt");

            if (!File.Exists(puzzleInputPath))
            {
                DownloadPuzzleInput(year, day, puzzleInputPath);
            }

            Console.WriteLine("Reading puzzle input from file {0}.", puzzleInputPath);

            return File.ReadAllText(puzzleInputPath);
        }

        private static string ProvisionDataDirectory(int year, int day)
        {
            var dataDirectory = Path.Combine(
                Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                "aoc",
                year.ToString(),
                day.ToString());

            // Create the data directory and all subdirectories automatically.
            try
            {
                // The CreateDirectory method will create all the directories
                // only if they do not already exist. No need to use Directory.Exists.
                var directoryInfo = Directory.CreateDirectory(dataDirectory);
                Console.WriteLine("The data directory was created successfully at {0}.", directoryInfo.FullName);
            }
            catch (IOException e)
            {
                Console.WriteLine("Could not create data directory: {0}", e.ToString());
            }

            return dataDirectory;
        }

        private static void DownloadPuzzleInput(int year, int day, string puzzleInputPath)
        {
            var aocPuzzleInputUrl = AocPuzzleInputUrl(year, day);
            var aocSessionCookie = AocSessionCookie();

            using (var httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Add("Cookie", aocSessionCookie);

                using (var httpResponse = httpClient.GetAsync(aocPuzzleInputUrl).Result)
                {
                    httpResponse.EnsureSuccessStatusCode();

                    var puzzleInputFileBytes = httpResponse.Content.ReadAsByteArrayAsync().Result;
                    File.WriteAllBytes(puzzleInputPath, puzzleInputFileBytes);

                    Console.WriteLine("Downloaded new puzzle input from {0} and saved to {1}.", aocPuzzleInputUrl, puzzleInputPath);
                }
            }
        }

        private static string AocPuzzleInputUrl(int year, int day)
        {
            return string.Format("https://adventofcode.com/{0}/day/{1}/input", year, day);
        }

        private static string AocSessionCookie()
        {
            var sessionCookieValue = Environment.GetEnvironmentVariable("AOC_SESSION_COOKIE");

            if (string.IsNullOrEmpty(sessionCookieValue))
            {
                var aocSessionCookiePath = Path.Combine(
                    Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                    "aoc",
                    "session.cookie");
                sessionCookieValue = File.ReadAllText(aocSessionCookiePath);
            }

            return string.Format("session={0}", sessionCookieValue);
        }
    }
}
